module Rand where

type RandData = (Numero, Seed)
type Numero = Int
type Seed = (Int, Int, Int)

randInt :: RandData -> Int
randInt (n, (a, c, m)) = mod (a*n + c) m

initRand :: RandData
initRand = (randInt (0, s), s)
    where s = (1103515245, 12345, 32768)
