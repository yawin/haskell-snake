module GameData (Snake, GameState, newGame, inicializaSerpiente, avanzaSerpiente, cambiaDireccion, borraSerpiente) where

import ScreenData
import Rand

data Snake =  Snake {position :: Coord, size :: Int, direction :: Coord} deriving (Show)
type GameState = (Snake, Pantalla, RandData)

inicializaSerpiente :: Pantalla -> Snake
inicializaSerpiente p = Snake {position = (div (getAnchoPantalla p) 2, div (getAltoPantalla p) 2), size = 3, direction = (0, 1)}

avanzaSerpiente :: GameState -> GameState
avanzaSerpiente (s, p, (rn, rs)) = if valor <= 0 then (Snake {position = newPos, size = newTam, direction = (direction s)}, setValorEnPantalla newPos (size s) manzana, (randInt (randInt (rn, rs), rs), rs))
                                   else newGame (getAnchoPantalla p) (getAltoPantalla p)
            where newPos = _avanza (position s) (direction s) (getAnchoPantalla p) (getAltoPantalla p)
                  newTam = if valor < 0 then (size s) + 1
                           else if valor > 0 then 0
                           else size s
                  valor = getValorEnPantalla newPos p
                  manzana = if newTam > (size s) then setValorEnPantalla randPoint (-1) p
                            else p
                  randPoint = (mod (randInt (rn, rs)) (getAnchoPantalla p), mod (randInt (randInt (rn, rs), rs)) (getAltoPantalla p))

_avanza :: Coord -> Coord -> Int -> Int -> Coord
_avanza (x, y) (dir_x, dir_y) w h = (_x, _y)
            where _x = if (x + dir_x >= w) then 0
                       else if (x + dir_x < 0) then w-1
                       else x + dir_x
                  _y = if (y + dir_y >= h) then 0
                       else if (y + dir_y < 0) then h-1
                       else y + dir_y

cambiaDireccion :: Coord -> Snake -> Snake
cambiaDireccion newDirec s = if newDirec /= (direccionInversa (direction s)) then Snake {position = (position s), size = (size s), direction = newDirec} else s

newGame :: Int -> Int -> GameState
newGame w h = avanzaSerpiente (inicializaSerpiente pantalla, setValorEnPantalla randPoint (-1) pantalla, (w*h, snd initRand))
      where pantalla = inicializaPantalla w h
            randPoint = (mod (randInt initRand) (getAnchoPantalla pantalla), mod (randInt (randInt initRand, snd initRand)) (getAltoPantalla pantalla))

direccionInversa :: Coord -> Coord
direccionInversa (x, y) = ((-x), (-y))

borraSerpiente :: Snake -> Snake
borraSerpiente s = Snake {position = (position s), size = -1, direction = (direction s)}
