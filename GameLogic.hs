module GameLogic where

import ScreenData
import GameData
import Rand

start :: IO()
start = gameLoop (newGame 42 10)

gameLoop :: GameState -> IO()
gameLoop (s, p, r) = do
                      draw (s, p, r)
                      c <- getChar
                      if c /= 'q' then
                        gameLoop (update(control c (s, p, r)))
                      else
                        putStr "\ESC[2J"

control :: Char -> GameState -> GameState
control 'w' (s, p, r) = (cambiaDireccion (0, (-1)) s, p, r)
control 's' (s, p, r) = (cambiaDireccion (0, 1) s, p, r)
control 'd' (s, p, r) = (cambiaDireccion (1, 0) s, p, r)
control 'a' (s, p, r) = (cambiaDireccion ((-1), 0) s, p, r)
--control 'q' (s, p, r) = borraPartida s p r
control _ (s, p, r) = (s, p, r)

update :: GameState -> GameState
update (s, p, r) = avanzaSerpiente (s, actualizaPantalla p, r)

draw :: GameState -> IO()
draw (_, p, _) = drawScreen p

borraPartida :: Snake -> Pantalla -> RandData -> GameState
borraPartida s p r = (borraSerpiente s, p, r)
