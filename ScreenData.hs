module ScreenData (Pantalla, Coord, drawScreen, getAnchoPantalla, getAltoPantalla, getValorEnPantalla, setValorEnPantalla, inicializaPantalla, actualizaPantalla) where

type Coord = (Int, Int)

suma :: Coord -> Coord -> Coord
suma (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

-----------------------------------------------------------------
-- PANTALLA
-----------------------------------------------------------------

data Casilla = Cas {position :: Coord, value :: Int} deriving Show
instance Eq Casilla where
  x == y = (position x) == (position y)

data Pantalla = Pan {width :: Int, height :: Int, casillero :: [Casilla]} deriving Show

drawScreen :: Pantalla -> IO()
drawScreen p = do
                clearScreen
                (putStr . concat . map (++"\n")) (getDraw p)

clearScreen :: IO()
clearScreen = putStr "\ESC[2J"

getDraw :: Pantalla -> [String]
getDraw p = ['—' | x <- [0..(width p)+1]] : ['|' : foldl (++) "" [getPixel (value (getCasilla (i, j) (casillero p))) | i <- [0..(width p)-1]] ++ "|" | j <- [0..(height p)-1]] ++ [['—' | x <- [0..(width p)+1]]]

getPixel :: Int -> String
getPixel v
    | v > 0 = "#"
    | v == 0 = " "
    | otherwise = "*"

getAnchoPantalla :: Pantalla -> Int
getAnchoPantalla p = width p

getAltoPantalla :: Pantalla -> Int
getAltoPantalla p = height p

getValorEnPantalla :: Coord -> Pantalla -> Int
getValorEnPantalla (x, y) p
    | x < 0 || x >= (width p) || y < 0 || y >= (height p) = -1
    | otherwise = value (getCasilla (x, y) (casillero p))

getCasilla :: Coord -> [Casilla] -> Casilla
getCasilla _ [] = Cas {position=(-1, -1), value=0}
getCasilla e (c:lc) = if (position c) == e then c else getCasilla e lc

setValorEnPantalla :: Coord -> Int -> Pantalla -> Pantalla
setValorEnPantalla c v p = Pan {width = (width p), height = (height p), casillero = (setCasilla c v (casillero p))}

setCasilla :: Coord -> Int -> [Casilla] -> [Casilla]
setCasilla _ _ [] = []
setCasilla pos v (c:lc) = if (position c) == pos then
                            Cas {position=pos, value=v} : lc
                          else
                            c: setCasilla pos v lc

inicializaPantalla :: Int -> Int -> Pantalla
inicializaPantalla w h = Pan {width = w, height = h, casillero = [Cas {position=(x, y), value = 0} | x <- [0..(w-1)], y <- [0..(h-1)]]}

actualizaPantalla :: Pantalla -> Pantalla
actualizaPantalla p = Pan {width = (width p), height = (height p), casillero = (actualizaCasillas (casillero p))}

actualizaCasillas :: [Casilla] -> [Casilla]
actualizaCasillas [] = []
actualizaCasillas (c:lc) = if value c > 0 then
                              Cas {position = (position c), value = ((value c) - 1)} : actualizaCasillas lc
                            else
                              c : actualizaCasillas lc
